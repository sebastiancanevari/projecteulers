let prev1 = 0
let prev2 = 1
let i = 0
let sol = 0
while (i <= 4000000) {
    i = prev1 + prev2
    prev1 = prev2
    prev2 = i
    if (i%2===0){
        sol+=i
    }
}

console.log(sol)